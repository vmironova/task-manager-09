package ru.t1consulting.vmironova.tm;

import ru.t1consulting.vmironova.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}