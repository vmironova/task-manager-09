package ru.t1consulting.vmironova.tm.service;

import ru.t1consulting.vmironova.tm.api.ICommandRepository;
import ru.t1consulting.vmironova.tm.api.ICommandService;
import ru.t1consulting.vmironova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
