package ru.t1consulting.vmironova.tm.api;

public interface ICommandController {
    void showErrorArgument();

    void showErrorCommand();

    void showVersion();

    void showInfo();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

    void showWelcome();
}
