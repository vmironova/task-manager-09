package ru.t1consulting.vmironova.tm.api;

import ru.t1consulting.vmironova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
